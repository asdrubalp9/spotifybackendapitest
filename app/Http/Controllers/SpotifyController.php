<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SpotifyController extends Controller
{
    //
    public $token; 
    public $ArtistData; 

    public function processToken(){

        
        $response = Http::withBasicAuth( env('SPOTIFY_CLIENT_ID'),env('SPOTIFY_CLIENT_SECRET'))
                ->asForm()
                ->post( env('API_AUTH_ENDPOINT').'api/token', [
                    'client_id' => env('SPOTIFY_CLIENT_ID'),
                    'grant_type' => 'authorization_code',
                    'code' => request()->get('code'),
                    'redirect_uri' => env('API_ENDPOINT').'token',
            ]);
    
        return $response;

    }

    public function authorization(){
        $response = Http::withToken( $this->token )
            ->get( env('API_AUTH_ENDPOINT'),'authorize/', [
                'response_type' => 'code',
                'redirect_uri' => env('API_ENDPOINT').'token',
                'scope'=> 'user-read-private%20user-read-email&state=34fFs29kd09',
            ]);

        return $response;
    }
    public function token(){
        
        if( request()->has('error') ){
            $response = [
                'type'          => 'error',
                'responseType'  => 302,
                'msg'           => request()->get('error')
            ];
        }else{
            if( request()->has('code') ){
                try {
                    $response = $this->processToken();
                    $res = $response->json();
                    
                    switch (true) {
                        case $response->successful():
                            $response = [
                                'type'            => 'success',
                                'responseType'    => $response->status(),
                                'token'           => $res['access_token'],
                                'token_type'      => $res['token_type'],
                                'expires_in'      => $res['expires_in'],
                                'refresh_token'   => $res['refresh_token'],
                            ];
                        break;
                        default:
                            $response = [
                                'type'              => $res['error'],
                                'responseType'      => $response->status(),
                                'error_description' => $res['error_description'],
                            ];
                        break;
                    }
                } catch (\Throwable $th) {
                    dd($th);
                    $response = [
                        'type'              => $res['error'],
                        'responseType'      => $response->status(),
                        'error_description' => $res['error_description'],
                    ];
                }
            }else{
                $response = [
                    'type'          => 'error',
                    'responseType'  => 302,
                    'msg'           => 'El token de autorización no está presente.'
                ];
            }

        }
        
        
        return view( 'loginResult', ['response' => $response ] );
    }

    public function main(){
        
        if( request()->has('q') ){
            
            if( request()->has('token') ){
                
                $this->token = request()->get('token');
                
                $response = $this->fetchArtist();
                
            }else{
                $response = [
                    'type'          => 'error',
                    'responseType'  => 302,
                    'msg'           => 'El token es invalido, ingresa aqui para poder obtener uno: '.env('API_AUTH_ENDPOINT').'authorize?response_type=code&client_id='.env('SPOTIFY_CLIENT_ID').'&redirect_uri='.env('API_ENDPOINT').'token'
                ];
            }
        }else{
            
            $response = [
                'type'          => 'error',
                'responseType'  => 400,
                'msg' => 'Parametros inválidos, el formato es: /api/v1/albums?q=nombreDeLaBanda&token=xxx'
            ];
        }
        return response()->json($response, $response['responseType'] );
    }

    public function processArtistData($data){

        $info = [];

        foreach( $data['artists']['items'] as $artist ){

            $artistId = $artist['id'];
            
            $response = Http::withToken( $this->token )
                        ->get( env('SPOTIFY_API_ENDPOINT').'artists/'.$artistId.'/albums' );

            $albumData = $response->json();

            $discs = [];
            foreach($albumData['items'] as $album){
                $disc = [
                    "name"=> $album['name'],
                    "released"=> $album['release_date'],
                    "tracks"=> $album['total_tracks'],
                    "cover"=> $albumData['items'][0]['images'][0]
                ];
                $discs[] = $disc;
            }
            $info[ $artist['name'] ]['albums'][ $album['album_type'] ][] = $discs;
        }

        $this->ArtistData = $info;
    }

    public function fetchArtist(){
            
        try {
            
            $server = Http::withToken($this->token )
                        ->get( env('SPOTIFY_API_ENDPOINT').'search', [
                            'q' => request()->get('q'),
                            'type' => 'artist'
                        ]);

            if( $server->successful() ){
                $artists = $server->json();
                $this->processArtistData($artists);
            }

            $response = [
                'responseType'  => $server->status(),
                'type'          => $server->status() == 200 ? 'success': 'error',
            ];

            if( $server->status() == 200 ){
                $response['data'] = $this->ArtistData;
                $response['msg']  = 'Aquí tienes el resultado por tu busqueda de: '. request()->get('q'); 
            }
        } catch (\Throwable $th) {
            
            $response = [
                'responseType'  => 302,
                'type'          => 'error',
                'msg'           => 'El token es invalido, ingresa aqui para poder obtener uno: '.env('API_AUTH_ENDPOINT').'authorize?response_type=code&client_id='.env('SPOTIFY_CLIENT_ID').'&redirect_uri='.env('API_ENDPOINT').'token'
            ];
        }

        return $response;
    }
}
