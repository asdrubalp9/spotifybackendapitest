<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Información de ingreso</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="col-md-4 mx-auto pt-5">
            <div class="card shadow">
                <div class="card-header {{ $response['responseType'] == 200 ? 'bg-success':'bg-danger' }} text-light text-center">
                    <h3>
                        {{ $response['responseType'] == 200 ? 'Datos de ingreso':'Error al iniciar sesión' }}
                    </h3>
                </div>
                <ul class="list-group">
                    @if($response['responseType'] == 200)
                            <li class="list-group-item">
                                token: {{ $response['token'] }}
                            </li>
                            <li class="list-group-item">
                                token_type: {{ $response['token_type'] }}
                            </li>
                            <li class="list-group-item">
                                expires_in: {{ $response['expires_in'] }}
                            </li>
                            <li class="list-group-item">
                                refresh_token: {{ $response['refresh_token'] }}
                            </li>    
                    @else
                        <li class="list-group-item text-center">
                            {{ $response['error_description'] }}
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</body>
</html>