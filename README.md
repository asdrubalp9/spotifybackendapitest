
## Prueba de integración de BACKEND realizado con Laravel 8, para conectarse al API de Spotify y traer los discos de los artistas dependiendo de los resultados de la búsqueda.



## Modo de uso

Para consultar el servicio online puedes realizar las consultas al API 

    https://spotify.invertronica.cl/api/v1/albums?q=[artista]&token=[token]

Ejemplo:

    https://spotify.invertronica.cl/api/v1/albums?q=muse&token=xxx
   
Las  representan el token obtenido del API de Spotify, por lo que debes iniciar sesión en el para poder realizar las consultas, en el caso de no contar con uno, el sistema te proveerá con el enlace para poder iniciar sesión y obtener el token de autenticación.

Para obtener el token de Spotify, puedes iniciar sesión en Spotify en el este [enlace](https://accounts.spotify.com/authorize?response_type=code&client_id=d9e979a748e744d08076c332ec7f1dbb&redirect_uri=https://spotify.invertronica.cl/api/v1/token) desde el navegador y hacer clic en aceptar, luego veras una ventana con los datos del token en el cual podrás copiar el token y usarlo para realizar las consultas de los artistas

## Como correrlo en local

 - Clonas el repositorio en tu PC.
 - Instalas las dependencias con el comando: composer update
 - Creas una APP en la consola de SPOTIFY: https://developer.spotify.com/dashboard/applications
 - Copias el archivo **.env.example**, lo nombras **.env** y modificas los parámetros: 

	    API_ENDPOINT=ELENDPOINTLOCAL
	    SPOTIFY_CLIENT_ID=
	    SPOTIFY_CLIENT_SECRET=

 - Corres tu servidor local  y realizas las consultas


## Pendientes
- Crear unit tests
- Refrescar el token